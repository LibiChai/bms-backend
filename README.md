# BMS backend

Backend of https://bms.jobsintown.org

## Setup

you can setup a local backend by cloning the repo an running yarn.

```
git clone git@gitlab.com:pwrk/bewerbermanagement/bms-backend.git
cd bms-backend
yarn && yarn dev
```

you can create an admin user for your local backend by pointing your browser
to http://localhost:1337/admin or via command line: `yarn strapi admin:create`

```
strapi@strapi:~/Projects/xxx/bms-backend$ yarn strapi admin:create-user --firstname=MyFirstname --email=test@test.test --password=MySecret1
yarn run v1.22.19
$ strapi admin:create-user --firstname=MyFirstname --email=test@test.test --password=MySecret1
Successfully created new admin
Done in 3.72s.
```

The Users & Permissions plugin automatically generated a jwt secret and stored it in .env under the name
JWT_SECRET. If you need to run the backend on some other PORT or IP use

`STRAPI_PUBLIC_URL=http://1.2.3.4:1337/`
