'use strict';

/**
 *  job controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::job.job', ({ strapi }) => ({
  async find(ctx) {
    const { user } = ctx.state
    console.log("AUTH", user)
    ctx.query.publicationState = 'preview'
    const { data, meta } = await super.find(ctx);
    return { data, meta };
  },  
}));

