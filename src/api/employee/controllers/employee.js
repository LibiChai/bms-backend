'use strict';

/**
 * employee controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::employee.employee', ({ strapi}) => ({
  async find(ctx) {
    try {
      const company = await getCompanyId(ctx);    
      const filters = {
        company: {
          id: {
            $eq: company.id,
          },
        }
      }
      ctx.query.filters = filters
      ctx.query = { ...ctx.query, company: company.id };
      const { data, meta } = await super.find(ctx);
      return { data, meta };
    } catch(e) {
      return ctx.forbidden('authenticated user has no company', {
        user: {
          id: ctx.state.user.id,
          username: ctx.state.user.username
        }
      })
    }
  },
  async create(ctx) {
    const company = await getCompanyId(ctx);
    ctx.request.body.data.company = parseInt(company.id)
    
    
    const { data, meta } = await super.create(ctx);
    console.log("CONTROLLER", ctx.request.body);
    return { data, meta };
  }
}));


async function getCompanyId(ctx) {
    const { user } = ctx.state
    const { company } = await strapi.entityService.findOne('plugin::users-permissions.user', user.id, {
      populate: 'company'
    });
    return company
}