module.exports = {
    async beforeCreate(data) {
      const user = {
          email: data.params.data.email,
          username: data.params.data.username
      },
      context = ''; 
      // todo  user_id need get
      const { company } = await strapi.entityService.findOne('plugin::users-permissions.user', 1, {
        populate: 'company'
      });
      if (user.email) {
        user.company = company
        const userCreated =  await strapi.service('plugin::passwordless.passwordless_ext').createUser(user);
        const token =  await strapi.service('plugin::passwordless.passwordless').createToken(user.email, context);
        const tokenSent = await strapi.service('plugin::passwordless.passwordless').sendLoginLink(token);
        console.log("BEFORE CREATE EPLOYEE:",data,  userCreated , token,tokenSent)
      }
    },
    async afterCreate(data) {
      console.log("AFTER CREATE EPLOYEE:",data)
    },

}