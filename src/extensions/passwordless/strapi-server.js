 
module.exports = (plugin) => {
  plugin.services.passwordless_ext = ({strapi})=>{
    let basePasswordless = plugin.services.passwordless({strapi})
    return {
        async createUser (user){
            const userSettings = await basePasswordless.userSettings();
            const role = await strapi
              .query('plugin::users-permissions.role')
              .findOne({
                where: {type: userSettings.default_role}
              });
      
            let newUser = {
              email: user.email,
              username: user.username || user.email,
              role: {id: role.id},
            }
      
            let populate = ['role']
            if (user.company != undefined){
              newUser.company = {id: user.company.id}
              populate.push('company')
            }
            
            
            return strapi
              .query('plugin::users-permissions.user')
              .create({data: newUser, populate: populate});
          }
    }
  }
  return plugin
}