'use strict';

module.exports = {
  /**
   * An asynchronous register function that runs before
   * your application is initialized.
   *
   * This gives you an opportunity to extend code.
   */
  register(/*{ strapi }*/) { 
  },

  /**
   * An asynchronous bootstrap function that runs before
   * your application gets started.
   *
   * This gives you an opportunity to set up your data model,
   * run jobs, or perform some special logic.
   */
  bootstrap({ strapi }) {
    strapi.db.lifecycles.subscribe({
      models: ['plugin::passwordless.token'],
      async beforeCreate(data) {
        const pluginStore = getPluginStore();
        const { settings } = strapi.config.passwordless;
        const value = settings
        let config = await pluginStore.get({ key: 'settings' });
        await pluginStore.set({ key: 'settings', value });
        const userDetails = data.params.data;
      },
      async afterCreate(data) {
        console.log("AfterCreate", data.result)
      },
    })
  },
};


function getPluginStore() {
  return strapi.store({
    environment: '',
    type: 'plugin',
    name: 'passwordless',
  });
}
