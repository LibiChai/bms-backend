Here you find Demo data. It is assumed that the Frontent runs under localhost:9000 and the Backend under localhost:1337.

You can "import" them by:

note:: this will replace an existing DB

```
test -d .tmp||mkdir .tmp
cp demo-data/.tmp/data.db .tmp/data.db
rsync av demo-data/public/uploads/ uploads/
```

The database contains an Admin User for the Backend:

http://localhost:1337/admin
* User: demo@test.de
* Password: Demo0000

and a Company Owner for the Frontend:

http://localhost:9000
* User: demo
* Password: demo00
