module.exports = ({ env }) => ({
  apiToken: {
    salt: env('API_TOKEN_SALT', 'ELI1QjBVNyYoSFp2nNgdqdjgRM0c7X4y')
  },
  auth: {
    secret: env('ADMIN_JWT_SECRET', 'f06f99cefce37b008d6a648af94e3e8b'),
  },
});
