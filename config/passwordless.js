var fs = require('fs');

const textMail = fs.readFileSync('mails/de/invite.txt','utf8');
const htmlMail = fs.readFileSync('mails/de/invite.html','utf8');



module.exports = ({ env }) => ({
  settings: {
    enabled: true,
    object: env('PWL_SUBJECT', 'welcome to BMS'),
    createUserIfNotExists: true,
    token_length: 20,
    expire_period: 360000,
    confirmation_url: env('PWL_CONFIRMATION_URL', 'http://localhost:9000/#confirm'),
    from_name: env('PWL_FROM_NAME','BMS'),
    from_email: env('PWL_FROM_EMAIL', 'carsten@bleek.org'),
    response_email: 'carsten@bleek.org',
    message_text: textMail,
    message_html: htmlMail
  },
});