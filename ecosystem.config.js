const GITLAB_TOKEN = process.env.GITLAB_TOKEN ?? '';
const GITLAB_USER = process.env.GITLAB_USER ?? GITLAB_TOKEN;
// Your repository
const REPO = 'https://' + GITLAB_USER + ':' + GITLAB_TOKEN + '@gitlab.com/pwrk/bewerbermanagement/bms-backend.git';

module.exports = {
  apps : [{
    name: '@pwrk/bms',
    script: 'yarn',
    args: 'start',
    interpreter: '/bin/bash',
    env: {
      NODE_ENV: "development",
      PORT: 1337
    },
    env_production: {
      NODE_ENV: "production",
      PORT: 3001
    }
  }],
  
  deploy : {
    production: {
      user : 'yawik',
      host : 'pm2.jobsintown.de',
      ref  : 'origin/main',
      repo : REPO,
      path : '/home/yawik/bms.jobsintown.de',
      'pre-deploy-local' : 'rsync -a --delete build/ yawik@pm2.jobsintown.de:bms.jobsintown.de/source/build/',
      'post-deploy' : 'cd /home/yawik/bms.jobsintown.de/source/ && git pull && yarn && pm2 startOrRestart ecosystem.config.js --interpreter bash --env production'
    },
    development: {
      user : 'yawik',
      host : 'localhost',
      ref  : 'origin/main',
      repo : REPO,
      path : '/home/strapi/bms.jobsintown.de',
      'pre-deploy-local' : 'rsync -a --delete /home/strapi/backend/build/ /home/strapi/pm2/build/',
      'post-deploy' : 'pm2 startOrRestart ecosystem.config.js --interpreter bash --env development'
    }    
  }
};
